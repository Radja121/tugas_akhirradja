@extends('layouts.app')

@section('title', 'Product List')

@section('content')
<div class="container">
    <h1>Product List</h1>
    
    <!-- Search Form -->
    <form action="{{ route('products.index') }}" method="GET" class="mb-4">
        <div class="input-group">
            <input type="text" name="search" class="form-control" placeholder="Search products..." value="{{ request()->query('search') }}">
            <button type="submit" class="btn btn-primary">Search</button>
        </div>
    </form>

    <!-- Add Product Button -->
    <a href="{{ route('products.create') }}" class="btn btn-success mb-4">Add Product</a>

    <!-- Product Table -->
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Description</th>
                <th>Stock</th>
                <th>Unit</th>
                <th>Is Published</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($products as $product)
            <tr>
                <td>{{ $product->id }}</td>
                <td>{{ $product->name }}</td>
                <td>{{ $product->description }}</td>
                <td>{{ $product->stock }}</td>
                <td>{{ $product->unit }}</td>
                <td>{{ $product->is_publish ? 'Yes' : 'No' }}</td>
                <td>
                    <a href="{{ route('products.edit', $product->id) }}" class="btn btn-warning btn-sm">Edit</a>
                    <form action="{{ route('products.destroy', $product->id) }}" method="POST" style="display:inline-block;">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger btn-sm" onclick="confirmDelete(event)">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="d-flex justify-content-center">
        {{ $products->links('pagination::bootstrap-4') }}
        </div>
</div>

<script>
    function confirmDelete(event) {
        event.preventDefault();
        var form = event.target.form;
        var confirmed = confirm("Yakin mau di delete ?");
        if (confirmed) {
            form.submit();
        }
    }
</script>
@endsection
