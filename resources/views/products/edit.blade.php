@extends('layouts.app')

@section('title', 'Edit Product')

@section('content')
<div class="container">
    <h1>Edit Product</h1>

    <form action="{{ route('products.update', $product->id) }}" method="POST">
        @csrf
        @method('PUT')
        @include('products.partials.form')
        <button type="submit" class="btn btn-success">Update</button>
    </form>
</div>
@endsection
