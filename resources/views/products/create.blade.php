@extends('layouts.app')

@section('title', 'Add Product')

@section('content')
<div class="container">
    <h1>Add Product</h1>

    <form action="{{ route('products.store') }}" method="POST">
        @csrf
        @include('products.partials.form')
        <button type="submit" class="btn btn-success">Save</button>
    </form>
</div>
@endsection
