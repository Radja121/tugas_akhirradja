<div class="mb-3">
    <label for="name" class="form-label">Name</label>
    <input type="text" name="name" class="form-control" id="name" value="{{ old('name', $product->name ?? '') }}">
</div>
<div class="mb-3">
    <label for="description" class="form-label">Description</label>
    <textarea name="description" class="form-control" id="description">{{ old('description', $product->description ?? '') }}</textarea>
</div>
<div class="mb-3">
    <label for="stock" class="form-label">Stock</label>
    <input type="number" name="stock" class="form-control" id="stock" value="{{ old('stock', $product->stock ?? '') }}">
</div>
<div class="mb-3">
    <label for="unit" class="form-label">Unit</label>
    <input type="text" name="unit" class="form-control" id="unit" value="{{ old('unit', $product->unit ?? '') }}">
</div>
<div class="mb-3">
    <label for="is_publish" class="form-label">Is Published</label>
    <select name="is_publish" class="form-control" id="is_publish">
        <option value="1" {{ old('is_publish', $product->is_publish ?? '') == 1 ? 'selected' : '' }}>Yes</option>
        <option value="0" {{ old('is_publish', $product->is_publish ?? '') == 0 ? 'selected' : '' }}>No</option>
    </select>
</div>