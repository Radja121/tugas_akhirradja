<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $produk = Product::all();

        return new ProductResource(True, 'Data Produk Berhasil', $produk);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function store(ProductRequest $request)
    {
        try {
            $product = Product::create($request->all());

            return new ProductResource(true, 'Data Produk telah di tambahkan', $product);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        try{
            $product = Product::find($id);

            if($product){
                return new ProductResource(true, 'Data Produk telah di temukan', $product);
            }else{
                return response()->json([
                    'message'   => 'Data Produk tidak di temukan'
                ], 422);
            }
        } catch(\Exception $e){
            return response()->json([
                'message'   => 'Terjadi kesalahan'
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try {
            $product = Product::find($id);
    
            if ($product) {
                $product->update($request->all());
                return new ProductResource(true, 'Data Produk berhasil diperbarui', $product);
            } else {
                return response()->json(['message' => 'Data Produk tidak ditemukan'], 404);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Terjadi kesalahan: ' . $e->getMessage()], 500);
            }

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $product = Product::find($id);
        
            if ($product) {
                $product->delete();
                return response()->json(['message' => 'Data Produk berhasil dihapus'], 200);
            } else {
                return response()->json(['message' => 'Data Produk tidak ditemukan'], 404);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Terjadi kesalahan: ' . $e->getMessage()], 500);
        }
    }
}
