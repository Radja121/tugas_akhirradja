<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'description' => 'required|string',
            'stock' => 'required|integer',
            'unit' => 'required|string|max:50',
            'is_publish' => 'required|boolean',
        ];
    }

    public function update()
    {
        return $this->rules();
    }

    protected function failedValidation(Validator $validator) { 
        throw new HttpResponseException(
            response()->json([
                'code' => 422,
                'message' => 'all field is required, check your input!',
                'data' => $validator->errors()->all()
            ], 422)
        ); 
    }
}
