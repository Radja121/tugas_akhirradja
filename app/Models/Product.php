<?php

namespace App\Models;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $guarded = 'id';
    protected $table = 'products';
    protected $fillable = [
        'name',
        'description',
        'stock',
        'unit',
        'is_publish',

    ];
}
